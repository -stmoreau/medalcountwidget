export const sorter = (data, criteria) => {
  return data.sort(function(country1, country2) {
    if (country1[criteria] === country2[criteria]) {
      const fallbackCriteria = criteria === 'gold' ? 'silver' : 'gold';
      return country1[fallbackCriteria] > country2[fallbackCriteria]
        ? -1
        : country1[fallbackCriteria] > country2[fallbackCriteria] ? 1 : 0;
    } else {
      return country1[criteria] > country2[criteria] ? -1 : 1;
    }
  });
};

export const addTotalColumn = data => {
  return data.map((country, i) => {
    const medals = [country.gold, country.silver, country.bronze];
    const total = medals.reduce(function(total, num) {
      return total + num;
    });
    country.total = total;
    return country;
  });
};

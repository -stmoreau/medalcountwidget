import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { tableActions } from '../actions';
import Table from '../components/Table';
import Loader from '../components/Loader';
import Error from '../components/Error';

class TableContainer extends Component {
  componentDidMount() {
    this.props.actions.getData();
  }

  render() {
    const { loading, error, data, sortBy, actions } = this.props;
    const sortTable = actions.sortTable;
    const tableHeaderProps = headerTitle => {
      return { headerTitle, sortBy, sortTable };
    };
    const tenCountries = data => (data ? data.slice(0, 10) : []);
    const tableRowProps = tenCountries(data).map((country, i) => Object.assign({}, country, { id: i + 1 }));
    if (loading) {
      return <Loader />;
    } else {
      if (error) {
        return <Error error={error} />;
      } else {
        return <Table tableHeaderProps={tableHeaderProps} tableRowProps={tableRowProps} />;
      }
    }
  }
}

TableContainer.propTypes = {
  loading: PropTypes.bool.isRequired,
  data: PropTypes.array,
  error: PropTypes.string,
  sortBy: PropTypes.string.isRequired,
  actions: PropTypes.object.isRequired
};

export default connect(
  state => ({
    loading: state.tableReducer.loading,
    data: state.tableReducer.data,
    error: state.tableReducer.error,
    sortBy: state.tableReducer.sortBy
  }),
  dispatch => ({
    actions: bindActionCreators(tableActions, dispatch)
  })
)(TableContainer);

import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import * as reducers from './reducers';
import Table from './containers/Table';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';
const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const composeEnhancers = composeWithDevTools({
  name: 'Medal Count Widget'
});

export const initialize = (elementQuery, sortBy) => {
  const initState = {
    tableReducer: { sortBy: sortBy || 'gold', loading: true }
  };
  const store = createStoreWithMiddleware(
    combineReducers({
      ...reducers
    }),
    initState,
    composeEnhancers(applyMiddleware())
  );

  ReactDOM.render(
    <Provider store={store}>
      <Table />
    </Provider>,
    document.querySelector(elementQuery)
  );
};

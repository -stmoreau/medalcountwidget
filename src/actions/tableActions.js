import axios from 'axios';
import * as types from '../constants/ActionTypes';

const DATA_API = 'https://s3-us-west-2.amazonaws.com/reuters.medals-widget/medals.json';

export function getData() {
  return dispatch => {
    return axios
      .get(DATA_API)
      .then(function(response) {
        dispatch({
          type: types.GET_DATA_START
        });
        return response.data;
      })
      .then(function(data) {
        dispatch({
          type: types.GET_DATA_SUCCESS,
          data: data
        });
      })
      .catch(function(error) {
        dispatch({
          type: types.GET_DATA_ERROR,
          errorMessage: error.message
        });
      });
  };
}

export function sortTable(criteria) {
  return dispatch => {
    dispatch({
      type: types.SORT_TABLE,
      sortBy: criteria
    });
  };
}

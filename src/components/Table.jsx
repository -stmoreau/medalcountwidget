import React from 'react';
import PropTypes from 'prop-types';
import TableHeader from './TableHeader';
import TableRow from './TableRow';
import '../styles/screen.scss';

const Table = ({ tableHeaderProps, tableRowProps }) => {
  return (
    <div className="layout__container">
      <h1>MEDAL COUNT</h1>
      <div className="table">
        <div className="table__heading">
          <div className="table__row">
            <div className="table__head" />
            <div className="table__head" />
            <div className="table__head" />
            <TableHeader {...tableHeaderProps('gold')} />
            <TableHeader {...tableHeaderProps('silver')} />
            <TableHeader {...tableHeaderProps('bronze')} />
            <TableHeader {...tableHeaderProps('total')} />
          </div>
        </div>
        <div className="table__body">{tableRowProps.map((data, i) => <TableRow props={data} key={i} />)}</div>
      </div>
    </div>
  );
};

Table.propTypes = {
  tableHeaderProps: PropTypes.func,
  tableRowProps: PropTypes.arrayOf(PropTypes.object).isRequired
};

export default Table;

import React from 'react';
import loader from '../assets/loader.gif';

const Loader = () => <img src={loader} alt="loading..." />;

export default Loader;

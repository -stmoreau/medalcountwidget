import React from 'react';
import PropTypes from 'prop-types';

const Error = ({ error }) => {
  return (
    <div className="error">
      <span className="error__emoji" role="img" aria-label="sad">
        😞
      </span>
      <div className="error__title">There has been a problem getting the data.</div>
      <div className="error__description">{error}</div>
    </div>
  );
};

Error.propTypes = {
  error: PropTypes.string.isRequired
};

export default Error;

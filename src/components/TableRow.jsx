import React from 'react';
import PropTypes from 'prop-types';

const TableRow = ({ props }) => {
  const { id, code, gold, silver, bronze, total } = props;

  return (
    <div className="table__row">
      <div className="table__cell">{id}</div>
      <div className="table__cell">
        <div className={`table__flag ${code}`} />
      </div>
      <div className="table__cell strong">{code}</div>
      <div className="table__cell">{gold}</div>
      <div className="table__cell">{silver}</div>
      <div className="table__cell">{bronze}</div>
      <div className="table__cell strong">{total}</div>
    </div>
  );
};

TableRow.propTypes = {
  props: PropTypes.object.isRequired
};

export default TableRow;

import React from 'react';
import PropTypes from 'prop-types';

const TableHeader = ({ headerTitle, sortBy, sortTable = () => {} }) => {
  const active = sortBy === headerTitle ? 'active' : '';

  return (
    <div className={`table__head ${active}`} onClick={() => sortTable(headerTitle)}>
      <div className={headerTitle}>{headerTitle}</div>
    </div>
  );
};

TableHeader.propTypes = {
  headerTitle: PropTypes.string,
  sortBy: PropTypes.string,
  sortTable: PropTypes.func
};

export default TableHeader;

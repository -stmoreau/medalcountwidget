import * as types from '../constants/ActionTypes';
import { sorter, addTotalColumn } from '../helpers';

const initState = {
  data: [],
  loading: true,
  error: null,
  sortBy: 'gold'
};

const tableReducer = (state = initState, action) => {
  switch (action.type) {
    case types.GET_DATA_START:
      return {
        ...state,
        loading: true
      };
    case types.GET_DATA_SUCCESS:
      return {
        ...state,
        data: sorter(addTotalColumn(action.data), state.sortBy),
        loading: false
      };
    case types.GET_DATA_ERROR:
      return {
        ...state,
        error: action.errorMessage,
        data: [],
        loading: false
      };
    case types.SORT_TABLE:
      return {
        ...state,
        data: sorter(addTotalColumn(state.data), action.sortBy),
        sortBy: action.sortBy
      };
    default:
      return state;
  }
};

export default tableReducer;

# Widget

The widget is meant to be embedded on client’s websites during the Olympic games. The widget shows up to 10 countries that have won the most medals of a given kind in the Olympics games.

### Prerequisites

* node
* npm

### Setup for dev

* run `npm install` to install dependencies
* run `npm run dev` to spin up the devserver
* open `http://localhost:8080/`

### Setup for test

* run `npm install` to install dependencies
* run `npm run test` to run tests

### Features

* It can accept up to two parameters:
  * element_id – an id of the element that the module will use as its
    container.
  * sort - determines the type of medals to sort by. It can have one of the
    following values: 'total', 'gold', 'silver' and 'bronze'.
* If no sort parameter is passed the sort should be by gold medals.
* When ranking by total medals, ties are broken by most gold.
* When ranking by gold, ties are broken by most silver.
* When ranking by silver, ties are broken by most gold.
* When ranking by bronze, ties are broken by most gold.
* Users can click the column headers to re-sort the countries by gold, silver,
  bronze, or total medals won. With each sort the possibility exists that new
  countries will enter and depart the top 10. The medals data should not be
  re-fetched from the server when sort changes.
* Follows the given design

### Documentation

The project contains 4 directories: `config`, `public`, `src`, `test`.

#### config

`config` is meant to contain the configuration for the different types of environments we'll need to run our widget.

* `webpack.config.dev.js` for our development configuration
* `webpack.config.test.js` for our test configuration
* `mocha-webpack.opts` contains configuration for mocha
  Could add in future config for `production` environment with code splitting etc.

#### public

`public` contains the `index.html` where the widget is added for test purposes and the `Thomson Reuters` favicon.

Note1: `widget.initialize()`'s first parameter is a query selector for our widget element.

Note2: in this case it is initialized using 'gold' as the default sorting. `silver`, `bronze`, `total` can be used as well, as parameters. If no second parameter is passed to `widget.initialize()` it will default sorting to `gold`.

#### src

`src` contains all our source code.

It has a pretty standard structure using redux.

So it's divided into:

* actions
* assets
* components (dumb components/stateless components)
* constants
* containers (smart components/connected to redux)
* helpers
* reducers
* styles (used BEM with SASS)

#### test

`test` contains all tests.

Unfortunately I only had time to test my components.

Had i had more time I would have tested my actions, containers and reducers.

Also the `widget`'s initialize method would need some kind of testing

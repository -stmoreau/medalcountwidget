const config = require('./webpack.config.dev');

config.target = 'node';

config.mode = 'development';

module.exports = config;

import React from 'react';
import TableRow from '../../src/components/TableRow';

describe('<TableRow /> component', () => {
  let wrapper;

  beforeEach(
    () => (wrapper = shallow(<TableRow props={{ id: 5, code: 'FRA', gold: 4, silver: 4, bronze: 7, total: 15 }} />))
  );

  it('should render the <TableRow /> component', () => {
    expect(wrapper).to.have.length(1);
  });

  it('should render the country id', () => {
    expect(
      wrapper
        .find('.table__cell')
        .at(0)
        .text()
    ).to.equal('5');
  });

  it('should render the country flag', () => {
    expect(wrapper.find('.table__flag').hasClass('FRA')).to.equal(true);
  });

  it('should render the country name', () => {
    expect(
      wrapper
        .find('.table__cell')
        .at(2)
        .text()
    ).to.equal('FRA');
  });

  it('should render the gold medals', () => {
    expect(
      wrapper
        .find('.table__cell')
        .at(3)
        .text()
    ).to.equal('4');
  });

  it('should render the silver medals', () => {
    expect(
      wrapper
        .find('.table__cell')
        .at(4)
        .text()
    ).to.equal('4');
  });

  it('should render the bronze medals', () => {
    expect(
      wrapper
        .find('.table__cell')
        .at(5)
        .text()
    ).to.equal('7');
  });

  it('should render the total medals', () => {
    expect(
      wrapper
        .find('.table__cell')
        .at(6)
        .text()
    ).to.equal('15');
  });
});

import React from 'react';
import Table from '../../src/components/Table';
import TableHeader from '../../src/components/TableHeader';
import TableRow from '../../src/components/TableRow';
import mockData from '../mock/mock-data';

describe('<Table /> component', () => {
  let wrapper;

  beforeEach(() => (wrapper = shallow(<Table tableHeaderProps={sinon.spy()} tableRowProps={mockData} />)));

  it('should render the <Table /> component', () => {
    expect(wrapper).to.have.length(1);
  });

  it('should have a title', () => {
    expect(wrapper.find('h1')).to.have.length(1);
    expect(wrapper.find('h1').text()).to.equal('MEDAL COUNT');
  });

  it('should have exactly 4 <TableHeader /> components', () => {
    expect(wrapper.find(TableHeader)).to.have.length(4);
  });

  it('should have exactly 10 <TableRow /> components', () => {
    expect(wrapper.find(TableRow)).to.have.length(10);
  });

  it('should pass expected props to <TableRow />', () => {
    expect(
      wrapper
        .find(TableRow)
        .at(0)
        .props().props
    ).to.deep.equal({
      code: 'USA',
      gold: 9,
      silver: 7,
      bronze: 12
    });
  });
});

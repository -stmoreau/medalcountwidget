import React from 'react';
import Error from '../../src/components/Error';

describe('<Error /> component', () => {
  let wrapper;

  beforeEach(() => (wrapper = shallow(<Error error="Request failed with status code 403" />)));

  it('should render the <Error /> component', () => {
    expect(wrapper).to.have.length(1);
  });

  it('should have an `.error__emoji`', () => {
    expect(wrapper.find('.error__emoji')).to.have.length(1);
  });

  it('should include the expected error title', () => {
    expect(wrapper.find('.error__title').html()).to.equal(
      '<div class="error__title">There has been a problem getting the data.</div>'
    );
  });

  it('should include the error string', () => {
    expect(wrapper.find('.error__description')).to.have.length(1);
  });
});

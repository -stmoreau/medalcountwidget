import React from 'react';
import Loader from '../../src/components/Loader';

describe('<Loader /> component', () => {
  it('should render the component', () => {
    const wrapper = shallow(<Loader />);
    expect(wrapper).to.have.length(1);
  });
});

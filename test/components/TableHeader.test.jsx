import React from 'react';
import TableHeader from '../../src/components/TableHeader';

describe('<TableHeader /> component', () => {
  let wrapper;

  beforeEach(() => (wrapper = shallow(<TableHeader sortBy="gold" headerTitle="gold" sortTable={sinon.spy()} />)));

  it('should render the <TableHeader /> component', () => {
    expect(wrapper).to.have.length(1);
  });

  it('should have the headerTitle as gold', () => {
    expect(wrapper.find('.gold').text()).to.equal('gold');
  });

  it('should by default have the active class', () => {
    expect(wrapper.find('.table__head').hasClass('active')).to.equal(true);
  });

  describe('when sortBy is not gold', () => {
    it('should not have the active class', () => {
      let wrapper = shallow(<TableHeader sortBy="silver" headerTitle="gold" sortTable={sinon.spy()} />);
      expect(wrapper.find('.table__head').hasClass('active')).to.equal(false);
    });
  });

  describe('when the user clicks on gold medal', () => {
    it('should trigger the sortTable function', () => {
      let wrapper = mount(<TableHeader sortBy="silver" headerTitle="gold" sortTable={sinon.spy()} />);
      wrapper.find('.table__head').simulate('click');

      expect(wrapper.props().sortTable).to.be.calledOnce.and.to.be.calledWithExactly('gold');
    });
  });
});
